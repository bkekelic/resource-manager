﻿namespace ComputerInfo
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timerForResource = new System.Windows.Forms.Timer(this.components);
            this.lbCPU = new System.Windows.Forms.Label();
            this.lbRAM = new System.Windows.Forms.Label();
            this.lbDisk = new System.Windows.Forms.Label();
            this.lbNetwork = new System.Windows.Forms.Label();
            this.pb_CPURAM = new System.Windows.Forms.PictureBox();
            this.timerOnStart = new System.Windows.Forms.Timer(this.components);
            this.pbDiskNet = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pb_CPURAM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDiskNet)).BeginInit();
            this.SuspendLayout();
            // 
            // timerForResource
            // 
            this.timerForResource.Interval = 1000;
            this.timerForResource.Tick += new System.EventHandler(this.timerForResource_Tick);
            // 
            // lbCPU
            // 
            this.lbCPU.AutoSize = true;
            this.lbCPU.ForeColor = System.Drawing.Color.White;
            this.lbCPU.Location = new System.Drawing.Point(39, 115);
            this.lbCPU.Name = "lbCPU";
            this.lbCPU.Size = new System.Drawing.Size(0, 17);
            this.lbCPU.TabIndex = 0;
            // 
            // lbRAM
            // 
            this.lbRAM.AutoSize = true;
            this.lbRAM.ForeColor = System.Drawing.Color.White;
            this.lbRAM.Location = new System.Drawing.Point(152, 115);
            this.lbRAM.Name = "lbRAM";
            this.lbRAM.Size = new System.Drawing.Size(0, 17);
            this.lbRAM.TabIndex = 1;
            // 
            // lbDisk
            // 
            this.lbDisk.AutoSize = true;
            this.lbDisk.ForeColor = System.Drawing.Color.White;
            this.lbDisk.Location = new System.Drawing.Point(39, 236);
            this.lbDisk.Name = "lbDisk";
            this.lbDisk.Size = new System.Drawing.Size(0, 17);
            this.lbDisk.TabIndex = 2;
            // 
            // lbNetwork
            // 
            this.lbNetwork.AutoSize = true;
            this.lbNetwork.ForeColor = System.Drawing.Color.White;
            this.lbNetwork.Location = new System.Drawing.Point(154, 236);
            this.lbNetwork.Name = "lbNetwork";
            this.lbNetwork.Size = new System.Drawing.Size(0, 17);
            this.lbNetwork.TabIndex = 3;
            // 
            // pb_CPURAM
            // 
            this.pb_CPURAM.BackColor = System.Drawing.Color.Black;
            this.pb_CPURAM.Location = new System.Drawing.Point(12, 12);
            this.pb_CPURAM.Name = "pb_CPURAM";
            this.pb_CPURAM.Size = new System.Drawing.Size(222, 100);
            this.pb_CPURAM.TabIndex = 4;
            this.pb_CPURAM.TabStop = false;
            // 
            // timerOnStart
            // 
            this.timerOnStart.Interval = 3;
            this.timerOnStart.Tick += new System.EventHandler(this.timerOnStart_Tick);
            // 
            // pbDiskNet
            // 
            this.pbDiskNet.BackColor = System.Drawing.Color.Black;
            this.pbDiskNet.Location = new System.Drawing.Point(12, 134);
            this.pbDiskNet.Name = "pbDiskNet";
            this.pbDiskNet.Size = new System.Drawing.Size(222, 100);
            this.pbDiskNet.TabIndex = 5;
            this.pbDiskNet.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(266, 260);
            this.Controls.Add(this.pbDiskNet);
            this.Controls.Add(this.pb_CPURAM);
            this.Controls.Add(this.lbNetwork);
            this.Controls.Add(this.lbDisk);
            this.Controls.Add(this.lbRAM);
            this.Controls.Add(this.lbCPU);
            this.Name = "Form1";
            this.Text = "Resource Monitor";
            ((System.ComponentModel.ISupportInitialize)(this.pb_CPURAM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDiskNet)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer timerForResource;
        private System.Windows.Forms.Label lbCPU;
        private System.Windows.Forms.Label lbRAM;
        private System.Windows.Forms.Label lbDisk;
        private System.Windows.Forms.Label lbNetwork;
        private System.Windows.Forms.PictureBox pb_CPURAM;
        private System.Windows.Forms.Timer timerOnStart;
        private System.Windows.Forms.PictureBox pbDiskNet;
    }
}


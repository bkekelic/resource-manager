﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace ComputerInfo
{
    public partial class Form1 : Form
    {
        PerformanceCounter perfCPUCounter = new PerformanceCounter("Processor Information", "% Processor Time", "_Total");
        PerformanceCounter perfRAMCounter = new PerformanceCounter("Memory", "% Committed Bytes In Use");
        PerformanceCounter perfDiskCounter = new PerformanceCounter("PhysicalDisk", "% Disk Time", "_Total");
        PerformanceCounter perfNetworkCounter = new PerformanceCounter("Network Adapter", "Bytes Total/sec", "Realtek RTL8723BE 802.11 bgn Wi-Fi Adapter");
        PerformanceCounter perfBandwidthCounter = new PerformanceCounter("Network Adapter", "Current Bandwidth", "Realtek RTL8723BE 802.11 bgn Wi-Fi Adapter");

        Bitmap bmpCPURAM, bmpDiskNet;
        Graphics graphicsCPURAM, graphicsDiskNet;
        Monitors monitorRAM, monitorCPU, monitorDisk, monitorNet, monitorStart;

        int tempPosto = 0, counter = 0;
        bool getToEnd= false, startProgram = true; 
        Pen penLine;
        

        public Form1()
        {
            InitializeComponent();

            bmpCPURAM = new Bitmap(pb_CPURAM.Size.Width, pb_CPURAM.Size.Height);
            bmpDiskNet = new Bitmap(pbDiskNet.Size.Width, pbDiskNet.Size.Height);

            pb_CPURAM.Image = bmpCPURAM;
            pbDiskNet.Image = bmpDiskNet;
            

            penLine = new Pen(Color.Red, 2);

            monitorCPU = new Monitors(0, 0, new Point(0,0));
            monitorRAM = new Monitors(0, 0, new Point(120, 0));
            monitorDisk = new Monitors(0, 0, new Point(0, 0));
            monitorNet = new Monitors(0, 0, new Point(120, 0));
            monitorStart = new Monitors(0, 0, new Point(0, 0));

            timerOnStart.Start();
        }
        

        private void timerForResource_Tick(object sender, EventArgs e)
        {
            drawBackCircles();
            calculateMonitors();

            lbCPU.Text = "CPU: "+ monitorCPU.GetUsage() + " %";
            lbRAM.Text = "RAM: "+ monitorRAM.GetUsage() + " %";
            lbDisk.Text = "Disk: "+ monitorDisk.GetUsage() + " %";
            lbNetwork.Text = "Net: "+ monitorNet.GetUsage() + " %";

        }
        private void timerOnStart_Tick(object sender, EventArgs e)
        {
            drawBackCircles();
            calculateMonitors();
        }

        private int getCPUUsage()
        {
            return (int)perfCPUCounter.NextValue();
        }
        private int getRAMUsage()
        {
            return (int)perfRAMCounter.NextValue();
        }
        private int getDiskUsage()
        {
            int val = (int)perfDiskCounter.NextValue();
            if (val > 100) val = 100;
            return val;
        }
        private int getNetworkUsage()
        {
            float networkUsage, totalBytesSec, bandwidth;
            totalBytesSec = perfNetworkCounter.NextValue();
            bandwidth = perfBandwidthCounter.NextValue();
            networkUsage = ((totalBytesSec * 8) / bandwidth) * 100;
            return (int)networkUsage;
        }


        private void drawBackCircles()
        {
            graphicsCPURAM = Graphics.FromImage(bmpCPURAM);
            graphicsDiskNet = Graphics.FromImage(bmpDiskNet);

            SolidBrush brush = new SolidBrush(Color.Black);

            graphicsCPURAM.FillEllipse(brush, monitorCPU.GetPointStartingCircle().X, monitorCPU.GetPointStartingCircle().Y, 100, 100);
            graphicsCPURAM.FillEllipse(brush, monitorRAM.GetPointStartingCircle().X, monitorRAM.GetPointStartingCircle().Y, 100, 100);
            graphicsDiskNet.FillEllipse(brush, monitorDisk.GetPointStartingCircle().X, monitorDisk.GetPointStartingCircle().Y, 100, 100);
            graphicsDiskNet.FillEllipse(brush, monitorNet.GetPointStartingCircle().X, monitorNet.GetPointStartingCircle().Y, 100, 100);

            pb_CPURAM.Image = bmpCPURAM;
            graphicsCPURAM.Dispose();
            pbDiskNet.Image = bmpDiskNet;
            graphicsDiskNet.Dispose();
        }
        private void calculateMonitors()
        {
            graphicsCPURAM = Graphics.FromImage(bmpCPURAM);
            graphicsDiskNet = Graphics.FromImage(bmpDiskNet);

            if (startProgram == true)
            {
                monitorStart.SetAngle(getAngle(tempPosto));

                monitorStart.SetPointStartingCircle(new Point(0, 0));
                drawHandles(monitorStart, 1);
                monitorStart.SetPointStartingCircle(new Point(120, 0));
                drawHandles(monitorStart, 1);

                monitorStart.SetPointStartingCircle(new Point(0, 0));
                drawHandles(monitorStart, 2);
                monitorStart.SetPointStartingCircle(new Point(120, 0));
                drawHandles(monitorStart, 2);
                

            } 
            else
            {
                monitorCPU.SetUsage(getCPUUsage());
                monitorRAM.SetUsage(getRAMUsage());
                monitorDisk.SetUsage(getDiskUsage());
                monitorNet.SetUsage(getNetworkUsage());

                monitorCPU.SetAngle(getAngle(monitorCPU.GetUsage()));
                monitorRAM.SetAngle(getAngle(monitorRAM.GetUsage()));
                monitorDisk.SetAngle(getAngle(monitorDisk.GetUsage()));
                monitorNet.SetAngle(getAngle(monitorNet.GetUsage()));

                drawHandles(monitorCPU, 1);
                drawHandles(monitorRAM, 1);
                drawHandles(monitorDisk, 2);
                drawHandles(monitorNet, 2);

            }
            
           
            pb_CPURAM.Image = bmpCPURAM;
            graphicsCPURAM.Dispose();
            pbDiskNet.Image = bmpDiskNet;
            graphicsDiskNet.Dispose();

            //pocetak programa kada se kazaljke okrecu
            if (startProgram == true)
            {
                if (tempPosto == 100) getToEnd = true;

                if (getToEnd == false)
                {
                    tempPosto++;
                }
                else
                {
                    tempPosto--;
                    if (tempPosto == -1)
                    {
                        startProgram = false;
                        timerOnStart.Stop();
                        timerForResource.Start();
                    }
                }
                    
            }
           
        }
        
        private void drawHandles(Monitors monitor, int code)
        {
            //code = 1 - cpu i ram, code = 2 disk i network
            // X = centar.X - polumjer * sin(kut)
            // Y = centar.Y + polumjer * cos(kut)

            Point pointOnCircle = new Point();
            int polumjer = 50, times;
            times = (int)monitor.GetAngle() - 2;

            if (startProgram) polumjer = 51;

            if(counter%2 == 0)
            {

            }

            for (int i = times; i < monitor.GetAngle(); i++)
            {
                pointOnCircle.X = (int)((monitor.GetPointStartingCircle().X + polumjer) - polumjer * Math.Sin(i * (Math.PI / 180)));
                pointOnCircle.Y = (int)((monitor.GetPointStartingCircle().Y + polumjer) + polumjer * Math.Cos(i * (Math.PI / 180)));

                if (monitor.GetAngle() >= 324) penLine.Color = Color.Red;
                else if (monitor.GetAngle() >= 234) penLine.Color = Color.Orange;
                else penLine.Color = Color.Green;

                if (code == 1) graphicsCPURAM.DrawLine(penLine, monitor.GetPointStartingCircle().X + polumjer, monitor.GetPointStartingCircle().Y + polumjer, pointOnCircle.X, pointOnCircle.Y);
                else graphicsDiskNet.DrawLine(penLine, monitor.GetPointStartingCircle().X + polumjer, monitor.GetPointStartingCircle().Y + polumjer, pointOnCircle.X, pointOnCircle.Y);

            }

        }
     

        private double getAngle(int posto)
        {
            return ((double)posto * 3.6);
        }

       
    }

    public struct Monitors
    {
        private int usage;
        private double angle;
        private Point pointStartingCircle;

        public Point GetPointStartingCircle()
        {
            return pointStartingCircle;
        }

        public void SetPointStartingCircle(Point value)
        {
            pointStartingCircle = value;
        }

        public Monitors(int usage, double angle, Point pointStartingCircle)
        {
            this.usage = usage;
            this.angle = angle;
            this.pointStartingCircle = pointStartingCircle;
        }

        public double GetAngle()
        {
            return angle;
        }

        public void SetAngle(double value)
        {
            angle = value;
        }

        public int GetUsage()
        {
            return usage;
        }

        public void SetUsage(int value)
        {
            usage = value;
        }
    }
}
